package com.example.juanp.theguardiansnews.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.juanp.theguardiansnews.objects.Article;
import com.example.juanp.theguardiansnews.utils.QueryUtils;

import java.util.List;

public class ArticleLoader extends AsyncTaskLoader<List<Article>> {

    //Url
    private String webUrl;
    private int currentSection;

    //Constructor
    public ArticleLoader(Context context, String url, int currentSection) {
        super(context);
        webUrl = url;
        this.currentSection = currentSection;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    //Background loading
    @Override
    public List<Article> loadInBackground() {
        if (webUrl == null) {
            return null;
        }
        // Get the data from the JSON url
        List<Article> articles = QueryUtils.fetchArticleData(webUrl,currentSection);
        return articles;
    }
}