package com.example.juanp.theguardiansnews.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.juanp.theguardiansnews.objects.Article;
import com.example.juanp.theguardiansnews.R;

import java.util.List;

public class ArticleAdapter extends ArrayAdapter<Article> {

    //Context
    private Context mContext;

    //Constructor
    public ArticleAdapter(Context mContext, List<Article> articles) {
        super(mContext, 0, articles);
        this.mContext = mContext;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if there is an existing list item view (called convertView) that we can reuse,
        // otherwise, if convertView is null, then inflate a new list item layout.
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.article_item, parent, false);
        }

        //Get the item
        Article currentArticle = getItem(position);

        //Date and time converter
        getDateTime(currentArticle, listItemView);

        //Set section and title of the articles
        TextView textWebTitle = (TextView) listItemView.findViewById(R.id.text_article_title);
        textWebTitle.setText(currentArticle.getWebTitle());
        TextView textSection = (TextView) listItemView.findViewById(R.id.text_article_section);
        textSection.setText(currentArticle.getSectionName());

        //Set the author of the article, if there is no data will be setted as blank
        TextView textAuthor = (TextView) listItemView.findViewById(R.id.text_article_author);
        if (currentArticle.getAuthor().equals("")){
            textAuthor.setText("");
        }else textAuthor.setText(mContext.getString(R.string.author) + " " + currentArticle.getAuthor());

        //Get the image url and set with glide
        ImageView imageArticle = (ImageView) listItemView.findViewById(R.id.image_article_thumbnail);
        Glide.with(mContext).load(currentArticle.getThumbnail()).into(imageArticle);

        return listItemView;
    }

    //Method to obtain, convert and set the time and date
    private void getDateTime(Article currentArticle, View listItemView) {

        //Get the date with the json format
        String gettedDate = currentArticle.getWebPublicationDate();

        //Split the date and put in different strings, deleting 'Z' from the time string
        String[] parts = gettedDate.split("T");
        String convertedDate = parts[0];
        String convertedTime = parts[1].substring(0, parts[1].lastIndexOf("Z"));

        //Set the date and time
        TextView textDate = (TextView) listItemView.findViewById(R.id.text_article_date);
        textDate.setText(convertedDate);
        TextView textTime = (TextView) listItemView.findViewById(R.id.text_article_time);
        textTime.setText(convertedTime);
    }
}

